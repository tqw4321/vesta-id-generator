package com.robert.vesta.operator;

import lombok.extern.java.Log;

/**
 * http://www.runoob.com/java/java-operators.html
 * @author xiejinjun
 * @version 1.0 2017/4/26
 */

@Log
public class Test {
    /**
     * &：按位与
     * 说明：全1为1，否则为0
     */
    @org.testng.annotations.Test
    public void testName() throws Exception {
        int test = 24 & 23;
        System.out.println(test);
    }

    /**
     * |：按位或
     * 说明：全0为0，否则为1
     */
    @org.testng.annotations.Test
    public void testOr() throws Exception {

    }
}
