package com.robert.vesta.converter;

import com.robert.vesta.service.bean.Id;
import com.robert.vesta.service.impl.bean.IdType;
import com.robert.vesta.service.impl.converter.IdConverter;
import com.robert.vesta.service.impl.converter.IdConverterImpl;
import org.testng.annotations.Test;

/**
 * @author xiejinjun
 */
public class IdConverteTest {

    private IdConverter maxPeakConverter = new IdConverterImpl(IdType.MAX_PEAK);

    private IdConverter minGranularityConverter = new IdConverterImpl(IdType.MIN_GRANULARITY);

    @Test
    public void testName() throws Exception {
        Id id = new Id();
        id.setType(0);
        id.setMachine(1);
        id.setSeq(1);
        id.setTime(12);
        id.setGenMethod(0);
        id.setVersion(1);
        maxPeakConverter.convert(id);
    }

    @Test
    public void testXx() throws Exception {
        System.out.println(3 << 2);
        System.out.println(3 << 3);
        System.out.println(3 << 4);
        System.out.println(3 << 5);
        System.out.println(3 << 6);
    }
}
