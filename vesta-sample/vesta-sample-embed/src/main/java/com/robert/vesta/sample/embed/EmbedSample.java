package com.robert.vesta.sample.embed;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.robert.vesta.service.bean.Id;
import com.robert.vesta.service.intf.IdService;

public class EmbedSample {
	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				"spring/vesta-service-sample.xml");
		IdService idService = (IdService) ac.getBean("idService");

		long id = idService.genId();
		Id ido = idService.expId(id);		
		//104321163953438721:Id(type=0, machine=1, seq=0, time=97156655, genMethod=0, version=0)
		long makeid = idService.makeId(ido.getVersion(), ido.getType(), ido.getGenMethod(), ido.getTime(), ido.getSeq(), ido.getMachine());
		System.out.println(id + ":" + ido +" makeid:"+makeid );
	}
}
